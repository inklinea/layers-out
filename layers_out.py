#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
#  Layers Out - An Inkscape Extension
#  Appears in 'Extensions>Export
#  Does not support nested layers
#  Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to inklinea.layers_out.noprefs
#  For shortcut triggered objects to new layer with last settings
#  Requires Inkscape 1.1+ -->
# #############################################################################


import inkex

from copy import deepcopy

import time
import sys
import os

def get_attributes(self):
    for att in dir(self):
        try:
            inkex.errormsg((att, getattr(self, att)))
        except:
            None

def make_layer_list(self, copied_svg):

    layer_list = []
    visible_layer_list = []
    document_layers = []

    if self.options.selected_layers_cb == 'true':
        # Now lets handle the new Inscape 1.2 ability to select layers
        my_selection = self.svg.selected
        for item in my_selection:
            if item.TAG == 'g':
                if item.get('inkscape:groupmode') == 'layer':
                    layer_id = item.get_id()
                    document_layers.append(copied_svg.getElementById(layer_id))
        if len(document_layers) < 1:
            # If we don't find any Inkscape 1.2 selected layers find all layers
            document_layers = copied_svg.xpath('//svg:g[@inkscape:groupmode="layer"]')
    else:
        document_layers = copied_svg.xpath('//svg:g[@inkscape:groupmode="layer"]')

    if len(document_layers) < 1:
        inkex.errormsg('No Layers Found')
        sys.exit()

    for layer in document_layers:
        layer_list.append(layer)
        if 'display' in layer.style.keys():
            if layer.style['display'] == 'none':
                continue
            else:
                visible_layer_list.append(layer)
        else:
            visible_layer_list.append(layer)

    return layer_list, visible_layer_list


def export_layers(self, copied_svg, document_layers, mode):

    output_folder = self.options.output_folder

    # Make a copy svg with all groups stripped out
    shell_svg = deepcopy(copied_svg)
    current_layers = shell_svg.xpath('//svg:g[@inkscape:groupmode="layer"]')

    for layer in current_layers:
        layer.delete()
    # Now all layers in shell_svg should be gone

    if mode == 'visible':
        layer_list = document_layers[1]
    elif mode == 'make_visible':
        for layer in document_layers[0]:
            if 'display' in layer.style.keys():
                layer.style.pop('display')

        layer_list = document_layers[0]
    else:
        layer_list = document_layers[0]

    for layer in layer_list:
        layer_id = layer.get_id()
        layer_label = layer.label

        layer_filename = layer_label + '_' + str(time.time()).replace('.', '_') + '.svg'
        layer_filepath = os.path.join(output_folder, layer_filename)

        # Lets make a copy of the shell_svg so we can populate
        temp_svg = deepcopy(shell_svg)
        temp_svg.append(layer)

        # Export svg with just the one layer
        with open(layer_filepath, 'w') as new_svg:
            new_svg_string = temp_svg.tostring().decode('utf-8')
            new_svg.write(new_svg_string)
            new_svg.close()

class MyFirstExtension(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--output_folder", type=str, dest="output_folder", default=None)

        pars.add_argument("--layers_out_notebook", type=str, dest="layers_out_notebook", default=0)

        pars.add_argument("--visibility_radio", type=str, dest="visibility_radio", default='visible')

        pars.add_argument("--selected_layers_cb", type=str, dest="selected_layers_cb", default='false')

    def effect(self):

        output_folder = self.options.output_folder

        # Lets test to see if we can write to the output folder
        layer_filepath = os.path.join(output_folder, str(time.time()))
        try:
            try_file = open(layer_filepath, 'w')
            try_file.close()
            os.remove(layer_filepath)
        except:
            inkex.errormsg('Output filepath is not valid ( or non writeable ) ')
            sys.exit()
        # End of writeability test

        # Lets copy the svg before processing the copy
        copied_svg = deepcopy(self.svg)

        # Get all layers as (layers_list, visible_layer_list)
        document_layers = make_layer_list(self, copied_svg)


        # Pass the layer list to export function four options
        # all (ignore visibility), visible (only visible), make_visible (make all visible)
        layer_export_mode = self.options.visibility_radio

        export_layers(self, copied_svg, document_layers, mode=layer_export_mode)



if __name__ == '__main__':
    MyFirstExtension().run()

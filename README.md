# Layers Out

Layers Out - an Inkscape 1.1+ Extension

#  Layers Out - An Inkscape Extension
  Appears in 'Extensions>Export

▶ Does not support nested layers

▶ Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to inklinea.layers_out.noprefs

▶ For shortcut triggered objects to new layer with last settings

▶ Requires Inkscape 1.1+ 
